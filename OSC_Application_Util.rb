require 'watir'
require 'watir-scroll'
require 'faker'
require 'json'

require_relative './config'

module OSCNewMerchant

  # Makes new file for mock data to be stored that will be used in the OSC
  #   application.
  #
  # @return [String] The name of the file that was created
  def self.make_new_file

  end

  # Retrieves the data file and parses it as JSON.
  #
  # @param file_name [String] The name of the file (without the extension) in
  #   the `test_data`
  #   folder. If the string ends in `'.xml'` it assumes that the string is a
  #   path to the file.
  # @return [JSON] The content of the data file parsed as JSON
  def self.get_file(file_name)

  end

  # Creates Mock Data for inserting into the OSC Application fields.
  #
  # @return [JSON] The mock data that is created by the faker gem parsed as JSON
  def self.make_data
    @mock_data = {
      'corporate_info' => [
        'business_name' => Faker::SiliconValley.company,
        'address' => Faker::Address.street_address,
        'city' => 'Alpharetta',
        'zip' => '30005',
        'phone_number' => Faker::PhoneNumber.phone_number,
        'fax_number' => Faker::PhoneNumber.phone_number,
        'contact_first_name' => Faker::Name.first_name
      ],
      'location_info' => [
        'business_start_date' => Faker::Date.forward
      ],
      'tax_info' => [
        'tax_id' => Faker::Number.number(9)
      ],
      'owner1' => [
        'first_name' => Faker::Name.first_name,
        'last_name' => Faker::Name.last_name
      ],
      'owner2' => [
        'first_name' => Faker::Name.first_name,
        'last_name' => Faker::Name.last_name
      ]
    }
  end

  # Inputs the mock data into the application form in Online Sales Center.
  #
  # @return [] null
  def self.input_data

  end

  # Creates a number of new merchant applications using the form in OSC for use
  #   in applications like UNO and AEX
  #
  # @param number_of_apps [int] The number of applications the user would like to create
  # @return [array] The name(s) of all merchant applications that were created
  #   named after the date and time they were created. These names will be
  #   files that are placed in this project directory under
  #   'Merchant_Applications'
  #
  # @see make_new_file; make_data; input_data
  def self.make_new_apps(number_of_apps)

  end

  # Searches through applications made using this util for applications matching
  #   the owner name that is given.
  #
  # @param owner_name [string] The name of the owner that will be used to search
  #   for a matching application
  # @return [array] The names of the files that matched the owner name given
  #
  # @see get_file
  def self.search_applications_by_name(owner_name)

  end
end